#include <iostream>
#include <iomanip>

#include "CaptureProcessor.h"
#include "GestureProcessor.h"
#include "Utilities.h"
#include "ConfigurationController.h"
#include "ActionProcessor.h"

int main( int argc, const char** argv )
{
	try {
		CaptureProcessor* captureProcessor = new CaptureProcessor(0, true, true, true);

		//Wczytanie konfiguracji
		captureProcessor->getGestureProcessor()->setGestures(ConfigurationController::readGesturesFromFile("configuration.txt"));

		//Obs�uga komend
		string command;
		cout << PROMPT;
		getline(cin, command);
		while(command != "exit") {
			vector<string> commFrag = splitString(command, " ");

			if(commFrag[0] == "help")
				cout<< HELP_STRING <<endl;
			else if(commFrag[0] == "binarize") {
				try {
					int h1 = atoi(commFrag[1].c_str());
					int s1 = atoi(commFrag[2].c_str());
					int v1 = atoi(commFrag[3].c_str());
					int h2 = atoi(commFrag[4].c_str());
					int s2 = atoi(commFrag[5].c_str());
					int v2 = atoi(commFrag[6].c_str());
					captureProcessor->setBinarizeFactors(h1, s1, v1, h2, s2, v2);
				}
				catch(...){ cout<<"Incorrect function parameters: binarize"<<endl; }			
			}
			else if(commFrag[0] == "add" && commFrag[1] == "gesture"){
				try { 
					if(captureProcessor->getGestureProcessor()->findGestureByName(commFrag[2]) == NULL) {
						captureProcessor->getGestureProcessor()->saveActualGesture(commFrag[2]);
						ConfigurationController::saveGesturesToFile(captureProcessor->getGestureProcessor()->getGestures(), "configuration.txt");
						cout<<"The gesture was added successfully: "<<commFrag[2]<<endl;
					}
					else cout<<"[ERROR] Gesture with that name currently exist"<<endl;
				}
				catch(...) { cout <<"[ERROR] Error during saving gesture"<<endl; }
			}
			else if(commFrag[0] == "delete" && commFrag[1] == "gesture"){
				try { 
					captureProcessor->getGestureProcessor()->deleteGesture(commFrag[2]); 
					cout<<"The gesture was deleted successfully: "<<commFrag[2]<<endl;
				}
				catch(...) { cout <<"[ERROR] Error during deleting gesture"<<endl; }
			}
			else if(commFrag[0] == "list" && commFrag[1] == "gestures"){
				try { 
					cout<<setw(15)<<"GESTURE"<<setw(15)<<"ACTION"<<endl;
					cout<<"----------------------------------"<<endl;
					for(auto gesture : captureProcessor->getGestureProcessor()->getGestures()) 
						cout << setw(15)<< gesture.first->getName() << setw(15) << gesture.second <<endl;			
				}
				catch(...) { cout <<"[ERROR] Error during liisting gestures"<<endl; }
			}
			else if(commFrag[0] == "list" && commFrag[1] == "actions"){
				try { 
					cout<<setw(25)<<"AVAILABLE ACTIONS"<<endl;
					cout<<"------------------------------"<<endl;
					for(int i=0; i<AVAILABLE_ACTIONS_COUNTER; i++)
						cout<<setw(20)<<AVAILABLE_ACTIONS[i]<<endl;
				}
				catch(...) { cout <<"[ERROR] Error during listing gestures"<<endl; }
			}
			else if(commFrag[0] == "pin"){
				if(captureProcessor->getActionProcessor()->isActionAvailable(commFrag[1])) {
					Gesture* gesture = captureProcessor->getGestureProcessor()->findGestureByName(commFrag[2]);
					if(gesture != NULL) {
						captureProcessor->getGestureProcessor()->pinActionToGesture(gesture, commFrag[1]);
						cout<<"Action pined to: "<<gesture->getName()<<endl;
					} 
					else cout<<"[ERROR] Gesture not found"<<endl;
				} 
				else cout<<"[ERROR] Action not available"<<endl;
			}
			else if(commFrag[0] == "unpin"){
				Gesture* gesture = captureProcessor->getGestureProcessor()->findGestureByName(commFrag[1]);
				if(gesture != NULL) {
					captureProcessor->getGestureProcessor()->pinActionToGesture(gesture, "-");
					cout<<"Action unpined from: "<<gesture->getName()<<endl;
				} 
				else cout<<"[ERROR] Gesture not found"<<endl;
			}
			else if(commFrag[0] == "start"){
				captureProcessor->getActionProcessor()->startProcessingActions();
				cout<<"Action processing: ON"<<endl;
			}
			else if(commFrag[0] == "stop"){
				captureProcessor->getActionProcessor()->stopProcessingActions();
				cout<<"Action processing: OFF"<<endl;
			}
			else if(commFrag[0] == "set" && commFrag[1] == "tolerance"){
				captureProcessor->setBinaryTolerance(atoi(commFrag[2].c_str()));
				cout<<"Binarize tolerance changed to: "<<commFrag[2]<<endl;
			}
			else if (commFrag[0] == "camera" && commFrag[1] == "settings") {
				captureProcessor->showCameraSettingsWindow();
			}
			else 
				cout << "[ERROR] Unrecognized command: "<< commFrag[0] <<endl;			
			cout << PROMPT;
			getline(cin, command);
		}
		
		//CleanUp
		ConfigurationController::saveGesturesToFile(captureProcessor->getGestureProcessor()->getGestures(), "configuration.txt");
		delete captureProcessor;
	}
	catch(string errorMsg) {
		cout<<"[ERROR] "<<errorMsg<<endl;
	}
	/*catch(...){
		cout<<"[ERROR] Unrecognized aplication error."<<endl;
	}*/

    return 0;
}