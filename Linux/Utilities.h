#pragma once
#include <vector>
#include <string>
#include <cstring>

using namespace std;

static const string PROMPT = "GestureRecognizer>> ";
static const string HELP_STRING = "------------------- HELP ------------------\nAvailable commands:\n - exit\t\t\t\t- close program,\n - help\t\t\t\t- print help,\n - binarize <H1> <S1> <V1> <H2> <S2> <V2> - new binarization rates\n - list gestures\t\t- list saved gestures\n - list actions\t\t\t- list available actions\n - set tolerance <value>\t- set binrization tolerance\n - camera settings\t\t- show camera settings window\n - add gesture <name>\t\t- save currently recognized gesture\n - pin <action> <gesture>\t- pin action to gesture\n - unpin <gesture>\t\t- unpin action from gesture\n - start\t\t\t- start action processing\n - stop\t\t\t\t- stop action processing\n--------------------------------------------";


vector<string> splitString(string, const char*);

class Utilities
{
private:
	static int SCREEN_WIDTH;
	static int SCREEN_HEIGHT;
	static void getDesktopResolution(int& horizontal, int& vertical);
public:
	Utilities(void);
	static int getScreenWidth();
	static int getScreenHeight();
	~Utilities(void);
};
