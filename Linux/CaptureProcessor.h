#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <thread>
#include <iostream>

#include "Utilities.h"
#include "GestureProcessor.h"
#include "ActionProcessor.h"

using namespace cv;
using namespace std;

class CaptureProcessor
{
private:
	bool running, isCameraShow, isBinaryShow, print;
	VideoCapture cap;
	int binH1, binH2, binS1, binS2, binV1, binV2;
	int binaryTolerance;
	std::thread* task;
	Mat frame, binary, dispFrame, dispBinary;
	GestureProcessor* gestureProcessor;
	ActionProcessor* actionProcessor;
	VideoCapture videoCapture;

public:
	CaptureProcessor(int camNumber, bool isCameraShow, bool isBinaryShow, bool print);
	void capture();
	void setBinarizeFactors(int, int, int, int, int, int);
	void setGestureProcessor(GestureProcessor*);
	void setActionProcessor(ActionProcessor*);
	void setBinaryTolerance(int tolerance);
	Mat getFrame();
	Mat getBinaryImage();
	void showCameraSettingsWindow();
	GestureProcessor* getGestureProcessor();
	ActionProcessor* getActionProcessor();
	static void mouseCallback(int, int, int, int, void*);
	void callBackFunc(int, int, int, int);
	~CaptureProcessor(void);
};

