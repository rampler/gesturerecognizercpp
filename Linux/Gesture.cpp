#include "Gesture.h"


Gesture::Gesture(std::string name, double feret, double blairBliss, double malinowska, double modMalinowska, double compactness)
{
	this->name = name; this->feret = feret; this->blairBliss = blairBliss;
	this->malinowska = malinowska; this->modMalinowska = modMalinowska; this->compactness = compactness;
}

Gesture::Gesture(Gesture* gesture) {
	this->name = gesture->getName();
	this->feret = gesture->getFeret();
	this->blairBliss = gesture->getBlairBliss();
	this->malinowska = gesture->getMalinowska();
	this->modMalinowska = gesture->getModMalinowska();
	this->compactness = gesture->getCompactness();
}

//Gettery
std::string Gesture::getName(){ return name; }
double Gesture::getFeret(){ return feret; }
double Gesture::getBlairBliss(){ return blairBliss; }
double Gesture::getMalinowska(){ return malinowska; }
double Gesture::getModMalinowska(){ return modMalinowska; }
double Gesture::getCompactness(){ return compactness; }

//Settery
void Gesture::setName(std::string name) { this->name = name; }

double Gesture::calculateDistanceToGesture(Gesture* gesture){
	//TODO: ewentualne przeskalowanie na warto�ci 0-1 wspolczynnikow w celu ich sprawiedliwosci 
	return sqrt(
				pow(feret-gesture->feret,2)+
				pow(blairBliss-gesture->blairBliss,2)+
				pow(malinowska-gesture->malinowska,2)+
				pow(modMalinowska-gesture->modMalinowska,2)+
				pow(compactness-gesture->compactness,2)
			);
}

Gesture::~Gesture(void)
{
}
