#pragma once
#include <list>
#include <map>
#include <thread>
#include "Gesture.h"
#include "GestureProcessor.h"
#include "MouseSimulator.h"
#include "HotKeysSimulator.h"

static const int FRAMES_RATE = 30;
static const int HOLD_FRAMES = 60;
static const int AVAILABLE_ACTIONS_COUNTER = 13;
static const string AVAILABLE_ACTIONS[AVAILABLE_ACTIONS_COUNTER] = {"ClickLMB","ClickRMB","PressLMB","PressRMB","ReleaseLMB","ReleaseRMB","ClickMB3","ClickMB4","ScrollUp","ScrollDown","Cut","Copy","Paste"};

class ActionProcessor
{
private:
	bool running;
	int* massCenter;
	int holdCounter;
	int frameWidth;
	int frameHeight;
	bool print;
	std::list<Gesture*> gesturesList;
	GestureProcessor* gestureProcessor;
	MouseSimulator* mouseSimulator;
	HotKeysSimulator* hotKeysSimulator;
public:
	ActionProcessor(GestureProcessor* gestureProcessor, bool print);
	void addNextFrameGesture(Gesture* gesture, int* massCenter, int frameWidth, int frameHeight);
	void startProcessingActions();
	void stopProcessingActions();
	bool isRunning();
	void processExecutingAction();
	void processAction(string actionName);
	bool isActionAvailable(string name);
	~ActionProcessor(void);
};

