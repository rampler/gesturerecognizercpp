#pragma once
//#include <Windows.h>

static const int SCROLL_LENGTH = 100;

class MouseSimulator
{
private:
	void getDesktopResolution(int& horizontal, int& vertical);
	int screenWidth;
	int screenHeight;
public:
	MouseSimulator(void);
	void setMousePosition(int x, int y);
	void clickLMB();
	void clickRMB();
	void pressLMB();
	void pressRMB();
	void releaseLMB();
	void releaseRMB();
	void clickMB3();
	void clickMB4();
	void scroll(int value);
	~MouseSimulator(void);
};

