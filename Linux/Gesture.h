#pragma once
#include <string>
#include <math.h>

//using namespace std;

class Gesture
{
private:
	std::string name;
	double feret, blairBliss, malinowska, modMalinowska, compactness;
public:
	Gesture(std::string, double feret, double blairBliss, double malinowska, double modMalinowska, double compactness);
	Gesture(Gesture* gesture);
	std::string getName();
	double getFeret();
	double getBlairBliss();
	double getMalinowska();
	double getModMalinowska();
	double getCompactness();
	void setName(std::string name);

	double calculateDistanceToGesture(Gesture* gesture);
	~Gesture(void);
};

