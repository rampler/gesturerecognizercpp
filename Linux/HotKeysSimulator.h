#pragma once
class HotKeysSimulator
{
public:
	HotKeysSimulator(void);
	void simulateCtrlX();
	void simulateCtrlC();
	void simulateCtrlV();
	~HotKeysSimulator(void);
};

