# GestureRecognizer #
C++ application to recognizing gestures.

--

Used libraries:

* OpenCV (http://opencv.org/) - BSD License

Version: 1.0.0

Release Date: 11.01.2016.

License: MIT

--

You can find polish version of project documentation in "Dokumentacja projektu.pdf" file.