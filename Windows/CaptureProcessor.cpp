#include "CaptureProcessor.h"

CaptureProcessor::CaptureProcessor(int camNumber, bool isCameraShow, bool isBinaryShow, bool print)
{
	VideoCapture cap(camNumber); //Domy�lna kamera
	videoCapture = cap;
    if(!cap.isOpened())  //Sprawdzenie, czy program ma dost�p do kamery
		throw "[ERROR] Aplication cannot connect with camera.";
	
	this->cap = cap;
	this->running = true;
	this->isCameraShow = isCameraShow;
	this->isBinaryShow = isBinaryShow;
	this->print = print;
	this->gestureProcessor = new GestureProcessor();
	this->actionProcessor = new ActionProcessor(gestureProcessor, print);
	binH1 = 0, binS1 = 0, binV1 = 0;
	binH2 = 100, binS2 = 100, binV2 = 100;
	binaryTolerance = 50;
	task = new std::thread(&CaptureProcessor::capture, this);
}

//G��wna funkcja pobieraj�ca obraz z kamery wywo�ywana w osobnym w�tku (*task)
void CaptureProcessor::capture() {
	Mat erodeElement = getStructuringElement(MORPH_RECT, cv::Size(3, 3));
	Mat dilateElement = getStructuringElement(MORPH_RECT, cv::Size(5, 5));

	//Wy�witlenie okna kamery
	if(isCameraShow) {
		namedWindow("Camera");
		moveWindow("Camera", 500,0);
		setMouseCallback("Camera", mouseCallback, this);
	}

	//Wy�wietlenie okna z obraz zbinaryzowanym
	if(isBinaryShow) {
		namedWindow("Binary");
		moveWindow("Binary", 820,0);
	}

	//Pobieranie kolejnych klatek z kamery
	while(running)
    {
		Mat tempBinary;
        cap >> frame; //Pobierz now� klatk� z kamery
		
		if(isCameraShow){
			resize(frame, dispFrame, Size(320,240));
			imshow("Camera", dispFrame); //Wyswietlenie aktualnego obrazu z kamery
		}

		//Binaryzacja obrazu
		cvtColor(frame, tempBinary, CV_BGR2HSV); 
		inRange(tempBinary, cv::Scalar(binH1, binS1, binV1), cv::Scalar(binH2, binS2, binV2), tempBinary);
		erode(tempBinary, tempBinary, erodeElement);
		dilate(tempBinary, tempBinary, dilateElement);
		binary = tempBinary;

		//Rozpoznawanie gest�w
		Gesture* gesture = gestureProcessor->processGestureRecognition(binary);
		int tempMassCenter[2] = {gestureProcessor->getMassCenter()[0], gestureProcessor->getMassCenter()[1]};
		actionProcessor->addNextFrameGesture(gesture, tempMassCenter, frame.size().width, frame.size().height);

		if(isBinaryShow) {
			resize(binary, dispBinary, Size(320,240));
			imshow("Binary", dispBinary);
		}

        if(waitKey(30) >= 0) break;
    }
}

//Ustawienie wsp�czynnik�w binaryzacji
void CaptureProcessor::setBinarizeFactors(int H1, int S1, int V1, int H2, int S2, int V2){
	binH1 = H1, binH2 = H2, binS1 = S1, binS2 = S2, binV1 = V1, binV2 = V2;
}

//Settery
void CaptureProcessor::setGestureProcessor(GestureProcessor* gestureProcessor){
	this->gestureProcessor = gestureProcessor;
}
void CaptureProcessor::setActionProcessor(ActionProcessor* actionProcessor){
	this->actionProcessor = actionProcessor;
}
void CaptureProcessor::setBinaryTolerance(int tolerance){
	this->binaryTolerance = tolerance;
}

void CaptureProcessor::showCameraSettingsWindow(){
	videoCapture.set(CV_CAP_PROP_SETTINGS, 1);
}

//Gettery
Mat CaptureProcessor::getFrame() { return frame; }
Mat CaptureProcessor::getBinaryImage() { return binary; }
GestureProcessor* CaptureProcessor::getGestureProcessor(){ return gestureProcessor; }
ActionProcessor* CaptureProcessor::getActionProcessor(){ return actionProcessor; }

//Obs�uga myszki
void CaptureProcessor::mouseCallback(int event, int x, int y, int flags, void *param)
{
    CaptureProcessor *self = static_cast<CaptureProcessor*>(param);
    self->callBackFunc(event, x, y, flags);
}

//Obs�uga klikni�cia myszki na oknie kamery
void CaptureProcessor::callBackFunc(int event, int x, int y, int flags)
{
     if  ( event == EVENT_LBUTTONDOWN )
     {
		 Mat hsvFrame;
		 cvtColor(dispFrame, hsvFrame, CV_BGR2HSV);
		 Vec3b pixel = hsvFrame.at<Vec3b>(Point(x,y));
		 int H1 = ((int)pixel.val[0]-(binaryTolerance/2) > 0)? (int)pixel.val[0]-(binaryTolerance/2) : 0;
		 int S1 = ((int)pixel.val[1]-binaryTolerance > 0)? (int)pixel.val[1]-binaryTolerance : 0;
		 int V1 = ((int)pixel.val[2]-binaryTolerance > 0)? (int)pixel.val[2]-binaryTolerance : 0;
		 int H2 = ((int)pixel.val[0]+(binaryTolerance/2) < 255)? (int)pixel.val[0]+(binaryTolerance/2) : 255;
		 int S2 = ((int)pixel.val[1]+binaryTolerance < 255)? (int)pixel.val[1]+binaryTolerance : 255;
		 int V2 = ((int)pixel.val[2]+binaryTolerance < 255)? (int)pixel.val[2]+binaryTolerance : 255;
		 setBinarizeFactors(H1, S1, V1, H2, S2, V2);
		 if(print)
			 cout<< "\nBinarization rates changed to: "<<H1<<" "<<S1<<" "<<V1<<" "<<H2<<" "<<S2<<" "<<V2<<endl<<PROMPT;
     }
}


CaptureProcessor::~CaptureProcessor(void)
{
	running = false;
	task->join();

	//Cleanup
	delete actionProcessor;
	delete gestureProcessor;
}
