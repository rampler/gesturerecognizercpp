#include "HotKeysSimulator.h"
#include <Windows.h>

HotKeysSimulator::HotKeysSimulator(void)
{
}

void HotKeysSimulator::simulateCtrlX(){
	keybd_event(VK_CONTROL, 0, 0, 0);
    keybd_event(0x58, 0, 0, 0); //Send the X key (58 is "X")
    keybd_event(0x58, 0, KEYEVENTF_KEYUP, 0);
    keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);
}
void HotKeysSimulator::simulateCtrlC(){
	keybd_event(VK_CONTROL, 0, 0, 0);
    keybd_event(0x43, 0, 0, 0); //Send the C key (43 is "C")
    keybd_event(0x43, 0, KEYEVENTF_KEYUP, 0);
    keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);
}
void HotKeysSimulator::simulateCtrlV(){
	keybd_event(VK_CONTROL, 0, 0, 0);
    keybd_event(0x56, 0, 0, 0); //Send the V key (56 is "V")
    keybd_event(0x56, 0, KEYEVENTF_KEYUP, 0);
    keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);
}

HotKeysSimulator::~HotKeysSimulator(void)
{
}
