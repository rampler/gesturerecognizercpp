#include "ActionProcessor.h"
#include "Utilities.h"

#include <iostream>

ActionProcessor::ActionProcessor(GestureProcessor* gestureProcessor, bool print)
{
	this->gestureProcessor = gestureProcessor;
	this->holdCounter = 0;
	this->running = false;
	this->print = print;
	mouseSimulator = new MouseSimulator();
	hotKeysSimulator = new HotKeysSimulator();
}

void ActionProcessor::addNextFrameGesture(Gesture* gesture, int* massCenter, int frameWidth, int frameHeight){ 
	this->massCenter = massCenter;
	this->frameWidth = frameWidth;
	this->frameHeight = frameHeight;

	if(gesturesList.size() > 30)
		gesturesList.pop_front();
	if(holdCounter == 0)
		gesturesList.push_back(gesture);
	else
		holdCounter--;

	if(running) 
		processExecutingAction();
}

void ActionProcessor::startProcessingActions(){ running = true; }
void ActionProcessor::stopProcessingActions(){ running = false; }
bool ActionProcessor::isRunning(){ return running; }

void ActionProcessor::processExecutingAction(){
	int newPositionX = massCenter[0];
	int newPositionY = massCenter[1];

	newPositionX = Utilities::getScreenWidth() - ((newPositionX-100)*Utilities::getScreenWidth()/(frameWidth-200));
	newPositionY = (newPositionY-100)*Utilities::getScreenHeight()/(frameHeight-200);
	mouseSimulator->setMousePosition(newPositionX, newPositionY); 

	if(gesturesList.size() >= FRAMES_RATE) { //Je�eli pula gestow jest wystarczajaco duza rozpoczynamy rozpoznawanie
		map<Gesture*, int> counters;
		for(Gesture* gest : gesturesList) 
			counters[gest]++;

		Gesture* gesture = NULL;
		int max = 31; //some big value
		for(auto gest: counters) {
			if(gest.second < max) {
				max = gest.second;
				gesture = gest.first;
			}
		}
		
		if(max > FRAMES_RATE/4 && gesture != NULL) { //Je�eli gest rozpoznany na min po�owie klatek
			if(print)
				cout<<"\nFiring action for: "<<((gesture != NULL)?gesture->getName():"null")<<" - "<<gestureProcessor->getGestures()[gesture]<<endl<<PROMPT;
			processAction(gestureProcessor->getGestures()[gesture]);
			gesturesList.clear();
			holdCounter = HOLD_FRAMES;
		}
	}
}

void ActionProcessor::processAction(string actionName) {
	if(actionName == "ClickLMB") mouseSimulator->clickLMB();
	else if(actionName == "ClickRMB") mouseSimulator->clickRMB();
	else if(actionName == "PressLMB") mouseSimulator->pressLMB();
	else if(actionName == "PressRMB") mouseSimulator->pressRMB();
	else if(actionName == "ReleaseLMB") mouseSimulator->releaseLMB();
	else if(actionName == "ReleaseRMB") mouseSimulator->releaseRMB();
	else if(actionName == "ClickMB3") mouseSimulator->clickMB3();
	else if(actionName == "ClickMB4") mouseSimulator->clickMB4();
	else if(actionName == "ScrollUp") mouseSimulator->scroll(100);
	else if(actionName == "ScrollDown") mouseSimulator->scroll(-100);
	else if(actionName == "Cut") hotKeysSimulator->simulateCtrlX();
	else if(actionName == "Copy") hotKeysSimulator->simulateCtrlC();
	else if(actionName == "Paste") hotKeysSimulator->simulateCtrlV();
}

bool ActionProcessor::isActionAvailable(string name) {
	for(int i=0; i<AVAILABLE_ACTIONS_COUNTER; i++)
		if(AVAILABLE_ACTIONS[i] == name)
			return true;
	return false;
}

ActionProcessor::~ActionProcessor(void)
{
	delete mouseSimulator;
	delete hotKeysSimulator;
}
