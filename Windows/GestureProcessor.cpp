#include "GestureProcessor.h"

#include <iostream> //TODO: to erase

GestureProcessor::GestureProcessor()
{
	actualGesture = new Gesture("",0,0,0,0,0);
	massCenter = new int[2];
	massCenter[0] = massCenter[1] = 0;
}

GestureProcessor::~GestureProcessor(void)
{
	for(auto gesture : gestures) 
		delete gesture.first;
}


//------------------------------------------------------------------------//
//------------------------- PRIVATE METHODS ----------------------------- //
//-----------------------------------------------------------------------//
vector<Point> GestureProcessor::findBiggestBlob(Mat binaryImage){
	Mat temp = binaryImage.clone();
    int largestArea=0;
    int largestContourIndex=0;
	Rect rect;

    vector<vector<Point>> contours; 
    vector<Vec4i> hierarchy;

    findContours(temp, contours, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	if(!contours.empty()) {
		for(int i = 0; i< contours.size(); i++) {
			double a=contourArea(contours[i],false);  
			if(a>largestArea){
				largestArea=a;
				largestContourIndex=i;               
			}
		}
		rect=boundingRect(contours[largestContourIndex]);
		rectangle(binaryImage, rect, Scalar(255));
		return contours[largestContourIndex];
	}
	return vector<Point>();
}

//------------------------------------------------------------------------//
//------------------------- PUBLIC METHODS ------------------------------ //
//-----------------------------------------------------------------------//

Gesture* GestureProcessor::processGestureRecognition(Mat binaryImage){
	vector<Point> blob = findBiggestBlob(binaryImage);
	if(!blob.empty()){
		double surfaceArea = contourArea(blob);
		double circuit = arcLength(blob,true);
		Rect rect = boundingRect(blob);
		RotatedRect rotatedRect = minAreaRect(blob);
		Moments moment = moments(blob, false);

		Point2f massCenter = Point2f(moment.m10/moment.m00, moment.m01/moment.m00);
		int tempMass[] = {massCenter.x, massCenter.y};
		this->massCenter = tempMass;

		//Wspolczynniki
		double distanceFromMassCenterSum = 0;
		for(int i=0; i<blob.size(); i++) 
			distanceFromMassCenterSum += pow(sqrt(pow(blob[i].x-massCenter.x,2)+pow(blob[i].y-massCenter.y,2)),2);

		double blairBliss = surfaceArea/(sqrt(2*PI*distanceFromMassCenterSum));
		double feret = (double)rect.height/(double)rect.width;
		double compactness = surfaceArea/(rotatedRect.size.width*rotatedRect.size.height);
		double malinowskiej = circuit/(2*sqrt(PI*surfaceArea)) - 1;
		double modMalinowskiej = (2*sqrt(PI*surfaceArea))/circuit;

		Gesture* tempGesture = actualGesture;
		actualGesture = new Gesture("", feret, blairBliss, malinowskiej, modMalinowskiej, compactness);
		delete tempGesture;

		double minDistance=100; //some big value
		Gesture* foundGesture = NULL;

		for(auto gesture : gestures) {
			double tempDist = gesture.first->calculateDistanceToGesture(actualGesture);
			if(tempDist < minDistance) {
				minDistance = tempDist;
				foundGesture = gesture.first;
			}
		}

		if(minDistance < TOLERANCE) 
			return foundGesture;
	}
	return NULL;
}

Gesture* GestureProcessor::saveActualGesture(string name) {
	Gesture* newGesture = new Gesture(actualGesture);
	newGesture->setName(name);
	gestures[newGesture] = "-";
	return newGesture;
}

void GestureProcessor::deleteGesture(string name) {
	Gesture* foundGesture;
	for(auto gesture : gestures)
		if(gesture.first->getName() == name) 
			foundGesture = gesture.first;
	if(foundGesture != NULL)
		gestures.erase(foundGesture);
}

Gesture* GestureProcessor::findGestureByName(string name) {
	for(auto gesture : gestures)
		if(gesture.first->getName() == name)
			return gesture.first;
	return NULL;
}

void GestureProcessor::pinActionToGesture(Gesture* gesture, string actionName) {
	gestures[gesture] = actionName;
}

map<Gesture*, string> GestureProcessor::getGestures(){ return gestures; }
int* GestureProcessor::getMassCenter(){ return massCenter; }
void GestureProcessor::setGestures(map<Gesture*, string> gestures){ this->gestures = gestures; }