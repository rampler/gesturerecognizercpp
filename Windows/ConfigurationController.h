#pragma once
#include <map>
#include <iostream>
#include <fstream>
#include "Gesture.h"

using namespace std;

class ConfigurationController
{
public:
	ConfigurationController(void);
	static map<Gesture*, string> readGesturesFromFile(string fileName);
	static void saveGesturesToFile(map<Gesture*, string> gestures, string fileName);
	~ConfigurationController(void);
};

