#include "ConfigurationController.h"


ConfigurationController::ConfigurationController(void)
{
}

map<Gesture*, string> ConfigurationController::readGesturesFromFile(string fileName){
	map<Gesture*, string> gestures;
	try {
		ifstream in(fileName);
		if(in.is_open()) {
			string line;
			while(getline(in, line)) {
				string name = line; getline(in,line);
				double feret = atof(line.c_str()); getline(in,line);
				double blairBliss = atof(line.c_str()); getline(in,line);
				double malinowaska = atof(line.c_str()); getline(in,line);
				double modMalinowska = atof(line.c_str()); getline(in,line);
				double compactness = atof(line.c_str()); getline(in,line);
				string actionName = line;

				Gesture* newGesture = new Gesture(name, feret, blairBliss, malinowaska, modMalinowska, compactness);

				gestures[newGesture] = actionName;
			}
			in.close();
		}
	}
	catch(...) { cout << "ERROR: Something goes wrong while reading configuration file!" << endl; }
	return gestures;
}

void ConfigurationController::saveGesturesToFile(map<Gesture*, string> gestures, string fileName){
	try {
		ofstream out(fileName);
		if(out.is_open()) {
			for(auto gesture : gestures) {
				out << gesture.first->getName() << endl;
				out << gesture.first->getFeret() << endl;
				out << gesture.first->getBlairBliss() << endl;
				out << gesture.first->getMalinowska() << endl;
				out << gesture.first->getModMalinowska() << endl;
				out << gesture.first->getCompactness() << endl;
				out << gesture.second <<endl;
			}
			out.close();
		}
	}
	catch(...) { cout << "ERROR: Something goes wrong while writing configuration file!" << endl; }
}

ConfigurationController::~ConfigurationController(void)
{
}
