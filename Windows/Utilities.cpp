#include "Utilities.h"
#include <wtypes.h>

vector<string> splitString(string command, const char* delimiter){
	vector<string> fragments;
	
	char *cstr = new char[command.length() + 1];
	strcpy(cstr, command.c_str());

	char* res = strtok(cstr, delimiter);

	while(res != NULL) {
		fragments.push_back(res);
		res = strtok(NULL, delimiter);
	}

	delete[] cstr;

	return fragments;
}

int Utilities::SCREEN_WIDTH = 0;
int Utilities::SCREEN_HEIGHT = 0;

void Utilities::getDesktopResolution(int& horizontal, int& vertical)
{
   RECT desktop;
   const HWND hDesktop = GetDesktopWindow();
   GetWindowRect(hDesktop, &desktop);
   horizontal = desktop.right;
   vertical = desktop.bottom;
}

int Utilities::getScreenWidth(){
	getDesktopResolution(SCREEN_WIDTH, SCREEN_HEIGHT);
	return SCREEN_WIDTH;
}

int Utilities::getScreenHeight(){
	getDesktopResolution(SCREEN_WIDTH, SCREEN_HEIGHT);
	return SCREEN_HEIGHT;
}