#pragma once
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <math.h>

#include "Gesture.h"

using namespace cv;
using namespace std;

static const double PI = 3.14159265;
static const double TOLERANCE = 0.5;

class GestureProcessor
{
private:
	map<Gesture*, string> gestures;
	Gesture* actualGesture;
	vector<Point> findBiggestBlob(Mat binaryImage);
	int* massCenter;
public:
	GestureProcessor();
	Gesture* processGestureRecognition(Mat binaryImage);
	Gesture* saveActualGesture(string name);
	void deleteGesture(string name);
	Gesture* findGestureByName(string name);
	void pinActionToGesture(Gesture* gesture, string actionName);

	map<Gesture*, string> getGestures();
	int* getMassCenter();

	void setGestures(map<Gesture*, string> gestures);
	~GestureProcessor(void);
};

